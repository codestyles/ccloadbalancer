const CONTAINERS_COUNT = 3;
const DISPATCHER_INTERVAL = 2000;
const CONTAINER_START_DELAY = 300;
const CONTAINER_RETRY_DELAY = 500;
const CONTAINER_CUSTOM_RUN_INTERVAL = 500;

const readline = require('readline');
const path = require("path");
const fs = require("fs");
const Docker = require('dockerode');
const cliProgress = require('cli-progress');

const docker = new Docker();
const pool = [];
const queue = [];
let doneTasks = 0;
let safeExit = [];
const bar1 = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


const runCont = async (i) => {
    await new Promise(resolve => {
        docker.run("ccproject", [], [], {
            Tty: true,
            AttachStdin: true,
            OpenStdin: true,
            name: "ccp_" + i,
            HostConfig: {
                Binds: [`${path.join(__dirname, "shared")}:/usr/local/src/ccproject/shared`]
            }
        }, (e, r, c) => {
            if (!safeExit[i]) {
                c.remove({force: true}, () => {
                    console.log(`Container ${i} stopped!`);
                });
            }
        }).on('container', function (container) {
            // console.log(`Container ${i} ran successfully.`);
            container.attach({stream: true, stdout: true, stderr: true, stdin: true}, function (err, stream) {
                setTimeout(() => {
                    const worker = {
                        i, stream, container, currentTask: null, tty: "",
                    };
                    stream.on('data', chunk => {
                        worker.tty += chunk;
                        if (!!worker.currentTask) {
                            const ttys = worker.tty.split("\n").map(t => t.trim());
                            if (ttys.includes("+D")) {
                                // Succeeded
                                worker.currentTask = null;
                                doneTasks++;
                                bar1.update(doneTasks);
                            } else if (ttys.includes("D")) {
                                // Failed
                                worker.currentTask = null;
                                doneTasks++;
                                bar1.update(doneTasks);
                            }
                        } else {
                            worker.tty = "";
                        }
                    });
                    if (pool[i]) {
                        pool[i] = worker;
                    } else {
                        pool.push(worker);
                        safeExit.push(false);
                    }
                    resolve();
                }, CONTAINER_START_DELAY);
            });
        });
    });
};

const mainThread = async () => {
    await new Promise(resolve => {
        docker.info(err => {
            if (!err) {
                console.log("Connected to docker engine successfully.");

                console.log("Building image...");
                docker.buildImage({
                    context: "ccproject",
                }, {t: "ccproject"}, (err, res) => {
                    if (!err) {
                        docker.modem.followProgress(res, async (err, res2) => {
                            console.log("Docker image built successfully.");
                            for (let i = 0; i < CONTAINERS_COUNT; i++)
                                await runCont(i);

                            console.log("Ready!");
                            runningLoop(resolve);
                        });

                    } else {
                        console.error(err);
                    }
                });

            } else {
                console.error(err);
            }
        });


    });
};

const countOfFreeWorkers = () => pool.filter(p => !p.currentTask).length;
// const getAnotherFreeWorker = () => pool.filter(p => !p.currentTask)[0];
const getAnotherFreeWorkerId = () => pool.filter(p => !p.currentTask)[0].i;

const safeDelayedRun = (func) => {
    const r = () => new Promise((resolve, reject) => func(resolve, reject));
    const r2 = () => {
        r().catch(() => {
            setTimeout(r2, CONTAINER_RETRY_DELAY);
        });
    };
    r2();
};

const dispatch = async (workerId, task) => {
    // console.log("Task " + task.command + " assigned to pool[workerId] " + pool[workerId].i);
    pool[workerId].currentTask = task;
    pool[workerId].tty = "";

    safeDelayedRun((resolve, reject) => {
        if (task.command.includes(".")) {
            pool[workerId].buildDir = "shared/build" + (new Date()).getTime() + Math.random().toString(36).substring(2, 10);
            pool[workerId].sourceFile = task.command.split("/").reverse()[0];
            pool[workerId].sourceType = pool[workerId].sourceFile.split(".").reverse()[0];
            fs.mkdirSync(pool[workerId].buildDir);
            fs.copyFileSync("./shared/" + task.command, "./" + pool[workerId].buildDir + "/" + pool[workerId].sourceFile);

            const monitor = (runnable) => {
                pool[workerId].runTimer = setInterval(() => {
                    runnable.inspect((e, d) => {
                        if (d && !d.Running) {
                            clearInterval(pool[workerId].runTimer);
                            try {
                                fs.copyFileSync("./" + pool[workerId].buildDir + "/out", task.output + "/" + pool[workerId].sourceFile.split(".")[0] + "-stdout.out");
                                fs.rmdirSync("./" + pool[workerId].buildDir, {recursive: true});
                            } catch (e) {
                            }
                            safeExit[workerId] = true;
                            pool[workerId].container.remove({force: true}, async () => {
                                await runCont(workerId);

                                doneTasks++;
                                bar1.update(doneTasks);
                            });
                        }
                    });
                }, CONTAINER_CUSTOM_RUN_INTERVAL);
            };

            let cmds = [];

            switch (pool[workerId].sourceType) {
                case "cpp":
                    cmds = ["/bin/bash", "-l", "-c", `cd /usr/local/src/ccproject/${pool[workerId].buildDir} && g++ ${pool[workerId].sourceFile} &> out  && ./a.out ${task.input} ${task.output} &>> out`];
                    break;
                case "py":
                    cmds = ["/bin/bash", "-l", "-c", `cd /usr/local/src/ccproject/${pool[workerId].buildDir} && pip3 install pipreqs && pipreqs . && pip3 install -r requirements.txt && python3 ${pool[workerId].sourceFile} ${task.input} ${task.output} &> out`];
                    break;
                default:
                    try {
                        fs.rmdirSync("./" + pool[workerId].buildDir, {recursive: true});
                    } catch (e) {
                    }
                    pool[workerId].currentTask = null;
                    resolve();
            }

            if (cmds.length > 0) {
                pool[workerId].container.exec({
                    Cmd: cmds,
                }, (e, runnable) => {
                    if (runnable) {
                        runnable.start(() => monitor(runnable));
                        resolve();
                    } else {
                        reject();
                    }
                });
            }

        } else {
            try {
                pool[workerId].stream.write(task.command + " " + task.input + " " + task.output + "\n");
                resolve();
            } catch (e) {
                reject();
            }
        }

    });
};

const schedulerThread = async () => {
    await new Promise(async resolve => {
        while (Math.min(queue.length, countOfFreeWorkers()) > 0) {
            await dispatch(getAnotherFreeWorkerId(), queue.shift());
        }
        resolve();
    });
}

const parseCommands = (cmd) => {
    cmd = cmd.trim();
    if (cmd.charAt(0) === "{" && cmd.charAt(cmd.length - 1) === "}") {
        try {
            const parts = cmd.substring(1, cmd.length - 1).split("<").join("").split(">").join("").split(",").map(p => p.trim()).filter(p => p.length > 0);
            const output = "./shared/" + parts.pop();
            const tasks = [];
            for (let i = 0; i < parts.length; i += 2) {
                tasks.push({
                    command: parts[i],
                    input: "./shared/" + parts[i + 1],
                    output
                });
            }
            return tasks;
        } catch (e) {
            return [];
        }
    } else {
        return [];
    }
};

const runningLoop = (resolve) => {
    rl.question(">", async (answer) => {
        switch (answer) {
            case "exit":
                console.log("\nExiting, please wait...");
                safeExit.fill(true);
                await Promise.all(pool.map(p => p.container.remove({force: true})));
                // await docker.getImage("ccproject").remove({force: true});
                rl.close();
                resolve();
                process.exit(0);
                return;
            case "status":
                console.log("Queue length: " + queue.length);
                console.log("Currently free workers: " + countOfFreeWorkers());
                console.log("Done tasks: " + doneTasks);
                break;
            default:
                const tasks = parseCommands(answer);
                queue.push(...tasks);
                if (tasks.length > 0) {
                    bar1.start(doneTasks + queue.length, doneTasks);
                    bar1.update(doneTasks);
                    // console.log("\n" + tasks.length + " new tasks added to queue.");
                    // console.log("Queue length: " + queue.length);
                }
        }

        runningLoop(resolve);
    });
};

mainThread();

setInterval(schedulerThread, DISPATCHER_INTERVAL);
