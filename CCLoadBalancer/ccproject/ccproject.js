const DEBUG_MODE = false;


const fs = require("fs");
const path = require("path");
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


const runningLoop = () => {
    rl.question("", (answer) => {
        if (answer === "exit") {
            return rl.close();
        }
        if (answer.split(" ").length === 3)
            try {
                handleRequest(...answer.split(" "));
            } catch (e) {}
        process.stdout.write("D\n");
        runningLoop();
    });
};

const handleRequest = (command, input, output) => {

    let result = "";

    if (command === "wordcount") {

        const words = fs.readFileSync(input).toString().split(/\s/).filter(w => w.trim().length > 0);

        const counts = [];
        /*
            counts = [
            [wsad, 1],
            [psd, 32],
            [gwefw, 2],
            ]
         */
        words.forEach(w => {
            const fltr = counts.filter(c => c[0] === w);
            if (fltr.length === 0) {
                counts.push([w, 1]);
            } else {
                fltr[0][1]++;
            }
        });

        result = counts.sort((a, b) => b[1] - a[1]).map(r => r.join(" ")).join("\n");
        // wsad 1
        // psd 32

    } else {

        const numbers = fs.readFileSync(input).toString().split("\n").filter(w => w.trim().length > 0).map(v => +v.trim());

        switch (command) {
            case "min":
                result = Math.min(...numbers);
                break;
            case "max":
                result = Math.max(...numbers);
                break;
            case "sort":
                result = numbers.sort((a, b) => b - a).join("\n");
                break;
            case "average":
                result = numbers.reduce((p, c) => p + c, 0) / numbers.length;
                break;
            default:
        }
    }

    if (DEBUG_MODE) {
        const d = (new Date().getTime());
        if (fs.existsSync(command + d + ".txt"))
            fs.rmSync(command + d + ".txt");
        fs.writeFileSync(path.join(output, command + d + ".txt"), "" + result);
    } else {
        fs.writeFileSync(path.join(output, command + ".txt"), "" + result);
    }
    process.stdout.write("+");
}

runningLoop();
